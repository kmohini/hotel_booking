// Copyright (c) 2016, Frappe and contributors
// For license information, please see license.txt

frappe.ui.form.on('Room Details',{

	refresh: function(frm) {

	},

	check_in_date:function(frm){
    		if (frm.doc.check_in_date < get_today()) 
   			{
        		frappe.msgprint("You can not select past date in Check In Date");
        		validated = false;
   			}
	},

	check_out_date:function(frm){
			check_in=frappe.datetime.str_to_obj(frm.doc.check_in_date);
			check_out=frappe.datetime.str_to_obj(frm.doc.check_out_date);
			diff=frappe.datetime.get_diff(check_out,check_in);
			if(diff<=0)
			{
				frappe.msgprint("Check Out Date Should be Greater than Check In Date");
			}
}

});
