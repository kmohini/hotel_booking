// Copyright (c) 2016, Frappe and contributors
// For license information, please see license.txt

frappe.ui.form.on('CustomerDetails', {
	refresh: function(frm) {

	},


	full_name:function(frm){

        var filter=/^[A-Za-z\s]+$/;
        if(!filter.test(frm.doc.full_name))
        {
            frappe.msgprint("Please Enter Characters Only");
        }
       
    },

mobile_number:function(frm){

        var filter=/^[1-9][0-9]{9}$/;
        if(!filter.test(frm.doc.mobile_number))
        {
            frappe.msgprint("Mobile Number Must Be Valid");
        }
    },





	
});
